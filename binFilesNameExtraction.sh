#!/bin/bash

git log --reverse > gitLogReverseOrder.txt

line=$(head -n 1 gitLogReverseOrder.txt)

IFS=' ' read -r -a array <<< "$line"
echo "${array[1]}" > theSHAthatWeNeeded.txt
